# TODO

## General

- [ ] Better resilience to failure to connect and disconnect
- [ ] Does not detect NATS disconnection
- [ ] Does not reconnect on NATS restart
- [ ] Add [CloudEvents](https://cloudevents.io/) as an envelop to the Visit events published to NATS
- [ ] Add a schema registry to track the evolution of the sechma of our Visit Events
- [ ] Refactor the microservice names (ingestion, estimator, publication) to something more meaningfull
- [ ] Split the `mono repo` into seperate git repos
- [ ] Could validate/sanitize the `user` field, to avoid things like SQL injection in `future` developpment.
- [ ] Automate Snyk in the CI/CD pipelines (NPM and Docker).
- [ ] Use a proper logging framework.
- [ ] complete the unit tests
- [ ] complete the integration and load tests
- [ ] IaC using Kubernetes manifest
- [ ] Add https support throught let's encrypt
- [ ] Add authentification/authorisation to the API (if needed)
- [ ] Secure all containers (non-root, remove default creds, etc)
- [ ] Add persistent volumes (to survive restart)
- [ ] implement event logging services to S3 (minio)
- [ ] implement Spark Job to correct the estimations
- [ ] add caching to the analytics API using MEMCACHED (or REDIS to simplify the infra)
      ...
