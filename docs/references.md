# References:

- [HyperLogLog in Practice: Algorithmic Engineering of a
  State of The Art Cardinality Estimation Algorithm](http://static.googleusercontent.com/media/research.google.com/en/us/pubs/archive/40671.pdf)
- [HyperLogLog: the analysis of a near-optimal cardinality estimation algorithm](http://algo.inria.fr/flajolet/Publications/FlFuGaMe07.pdf)
- [Big Data Counting: How To Count A Billion Distinct Objects Using Only 1.5KB Of Memory](http://highscalability.com/blog/2012/4/5/big-data-counting-how-to-count-a-billion-distinct-objects-us.html)
- [Loglog counting of large cardinalities](http://algo.inria.fr/flajolet/Publications/DuFl03-LNCS.pdf)
- [Redis bitmaps - fast, easy, realtime metrics](https://blog.getspool.com/2011/11/29/fast-easy-realtime-metrics-using-redis-bitmaps/)

## Implementations of HyperLogLog

[Java](https://github.com/addthis/stream-lib)
[NodeJS](https://github.com/optimizely/hyperloglog) default value of `n` yield less than 1.625% relative error (higher value of `n` uses more memory)
[REDIS](https://redis.io/commands/PFCOUNT) approximated with a standard error of 0.81%.
