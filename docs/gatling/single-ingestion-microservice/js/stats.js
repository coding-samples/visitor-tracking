var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "7196",
        "ok": "7177",
        "ko": "19"
    },
    "minResponseTime": {
        "total": "5",
        "ok": "5",
        "ko": "15"
    },
    "maxResponseTime": {
        "total": "2740",
        "ok": "2740",
        "ko": "1788"
    },
    "meanResponseTime": {
        "total": "540",
        "ok": "540",
        "ko": "655"
    },
    "standardDeviation": {
        "total": "753",
        "ok": "753",
        "ko": "516"
    },
    "percentiles1": {
        "total": "45",
        "ok": "45",
        "ko": "855"
    },
    "percentiles2": {
        "total": "1208",
        "ok": "1210",
        "ko": "867"
    },
    "percentiles3": {
        "total": "2012",
        "ok": "2012",
        "ko": "1682"
    },
    "percentiles4": {
        "total": "2237",
        "ok": "2238",
        "ko": "1767"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4930,
    "percentage": 69
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 439,
    "percentage": 6
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1808,
    "percentage": 25
},
    "group4": {
    "name": "failed",
    "count": 19,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "130.836",
        "ok": "130.491",
        "ko": "0.345"
    }
},
contents: {
"req_random-event-47c48": {
        type: "REQUEST",
        name: "random-event",
path: "random-event",
pathFormatted: "req_random-event-47c48",
stats: {
    "name": "random-event",
    "numberOfRequests": {
        "total": "7196",
        "ok": "7177",
        "ko": "19"
    },
    "minResponseTime": {
        "total": "5",
        "ok": "5",
        "ko": "15"
    },
    "maxResponseTime": {
        "total": "2740",
        "ok": "2740",
        "ko": "1788"
    },
    "meanResponseTime": {
        "total": "540",
        "ok": "540",
        "ko": "655"
    },
    "standardDeviation": {
        "total": "753",
        "ok": "753",
        "ko": "516"
    },
    "percentiles1": {
        "total": "45",
        "ok": "45",
        "ko": "855"
    },
    "percentiles2": {
        "total": "1208",
        "ok": "1210",
        "ko": "867"
    },
    "percentiles3": {
        "total": "2012",
        "ok": "2012",
        "ko": "1682"
    },
    "percentiles4": {
        "total": "2237",
        "ok": "2238",
        "ko": "1767"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4930,
    "percentage": 69
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 439,
    "percentage": 6
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1808,
    "percentage": 25
},
    "group4": {
    "name": "failed",
    "count": 19,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "130.836",
        "ok": "130.491",
        "ko": "0.345"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
