# Instructions

## REQUIREMENTS

> If you have not already installed the prerequisite, follow the [installation instructions here](installation.md). Also make sure that you have [all the required ports available](networking.md) in the networking section!

## Subtilities of Docker

Depending on your version of `Docker` the `compose` command can be either:

- `docker compose` (with a space)

> OR

- `docker-compose` (with a dash).

You will have to be mindful of that, if you simply copy & past the command from the instructions here.

## Let's start the whole shebang!

1. If it is not already done, clone the project:

```bash
$ git clone https://gitlab.com/coding-samples/visitor-tracking.git
$ cd visitor-tracking
```

2. Start _everything_

```bash
$ docker compose pull
$ docker compose up -d
[+] Running 8/8
 ⠿ Network "narrative_default"        Created
 ⠿ Container nats-streaming           Started
 ⠿ Container narrative_publication_1  Started
 ⠿ Container nginx                    Started
 ⠿ Container narrative_estimation_1   Started
 ⠿ Container redis                    Started
 ⠿ Container narrative_ingestion_1    Started
 ⠿ Container nats-streaming-console   Started
```

3. WAIT until everything is ready

I haven't implemented the dependencies between services. Therefore, a microservice could start before its dependency and fail. It will restart and eventually it will all be good. Before trying to make a request, you should monitor the stack with `docker ps` and wait for the 5th column (STATUS) to be `Up` for _ALL_ the components. It can take a minute or so at the initial startup.

```bash
$ docker ps
CONTAINER ID   IMAGE                             COMMAND                  CREATED          STATUS          PORTS                    NAMES
53006df02d74   nginx:1.21-alpine                 "/docker-entrypoint.…"   57 seconds ago   Up 46 seconds   0.0.0.0:8080->80/tcp     nginx
15e2a400e645   nats-streaming:0.21-alpine        "docker-entrypoint.s…"   57 seconds ago   Up 49 seconds   4222/tcp, 8222/tcp       nats-streaming
26d02ba69017   redis:6.2-alpine                  "docker-entrypoint.s…"   57 seconds ago   Up 49 seconds   6379/tcp                 redis
6ce178ffd50b   hbouvier/nats-streaming-console   "docker-entrypoint.s…"   57 seconds ago   Up 47 seconds   0.0.0.0:8282->8282/tcp   nats-streaming-console
6788dd66a762   hbouvier/ingestion                "docker-entrypoint.s…"   57 seconds ago   Up 50 seconds                            narrative_ingestion_1
a517e9f441af   hbouvier/estimation               "docker-entrypoint.s…"   57 seconds ago   Up 49 seconds                            narrative_estimation_1
c5f1739e2526   hbouvier/publication              "docker-entrypoint.s…"   57 seconds ago   Up 50 seconds                            narrative_publication_1
```

> At this point you should be able to see the [NATS MONITORING WEB UI](http://localhost:8282/).

4. Make a few clicks and impressions to the API

Make a few impressions

```bash
$ curl -vXPOST "http://localhost:8080/analytics?timestamp=$(date +%s)000&user=bob&event=impression"
$ curl -vXPOST "http://localhost:8080/analytics?timestamp=$(date +%s)000&user=john&event=impression"
$ curl -vXPOST "http://localhost:8080/analytics?timestamp=$(date +%s)000&user=bob&event=click"
$ curl -vXPOST "http://localhost:8080/analytics?timestamp=$(date +%s)000&user=steve&event=impression"
$ curl -vXPOST "http://localhost:8080/analytics?timestamp=$(date +%s)000&user=boby&event=impression"
$ curl -vXPOST "http://localhost:8080/analytics?timestamp=$(date +%s)000&user=boby&event=click"
$ curl -vXPOST "http://localhost:8080/analytics?timestamp=$(date +%s)000&user=boby&event=impression"
$ curl -vXPOST "http://localhost:8080/analytics?timestamp=$(date +%s)000&user=boby&event=impression"
```

5. Get the analytics result

Get the results

```bash
$ curl -v "http://localhost:8080/analytics?timestamp=$(date +%s)000"
```

```json
{
  "unique_users": 4,  # bob(2), john(1), steve(1) and boby(4). 8 total, 4 unique
  "clicks": 2,        # bob(1) and boby(1) did click
  "impressions": 6    # bob(1), john(1), steve(1), boby(3)
}
```

7.  Teardown

```bash
$ docker compose down
```

---

Alternatively, if you don't trust me and wants to build the code from this repository from the source code, run this command instead:

```bash
# Build all the images locally
$ docker compose build

# Scan them for vulnerability, *if you have an authenticated Snyk account*
$ docker scan registry.gitlab.com/coding-samples/visitor-tracking/ingestion
$ docker scan registry.gitlab.com/coding-samples/visitor-tracking/estimation
$ docker scan registry.gitlab.com/coding-samples/visitor-tracking/publication

# Start the whole stack like in step #2 of this document.
$ docker compose up -d
```

---

# Develop locally

1. Make sure you have cloned the repo

```bash
$ git clone https://gitlab.com/coding-samples/visitor-tracking.git
$ cd visitor-tracking
```

2. Start the infra in docker

```bash
$ docker compose -f docker-compose-dev.yml up -d redis nats-streaming nats-streaming-console
```

## Common modules

```bash
$ cd common
$ npm install
$ npm run initial-setup
$ npm run build
$ npm run test
$ npm run test-vulnerability  # you have to be authenticated with Snyk
$ cd ..
```

## Ingestion (POST /analytics)

```bash
$ cd ingestion
$ cp sample.env .env.dev
$ npm install
$ npm run initial-setup
$ npm run build
$ npm run test
$ npm run test-vulnerability  # you have to be authenticated with Snyk
$ npm run dev
```

## Estimation (worker)

In another terminal window

```bash
$ cd estimation
$ cp sample.env .env.dev
$ npm install
$ npm run initial-setup
$ npm run build
$ npm run test
$ npm run test-vulnerability  # you have to be authenticated with Snyk
$ npm run dev
```

## Publication (GET /analytics)

In another terminal window

```bash
$ cd publication
$ cp sample.env .env.dev
$ npm install
$ npm run initial-setup
$ npm run build
$ npm run test
$ npm run test-vulnerability  # you have to be authenticated with Snyk
$ npm run dev
```

## Use the API as in step #4 of this document

Make a few impressions (make sure you are using the right port)

```bash
$ curl -vXPOST "http://localhost:3000/analytics?timestamp=$(date +%s)000&user=bob&event=impression"
$ curl -vXPOST "http://localhost:3000/analytics?timestamp=$(date +%s)000&user=john&event=impression"
$ curl -vXPOST "http://localhost:3000/analytics?timestamp=$(date +%s)000&user=bob&event=click"
$ curl -vXPOST "http://localhost:3000/analytics?timestamp=$(date +%s)000&user=steve&event=impression"
$ curl -vXPOST "http://localhost:3000/analytics?timestamp=$(date +%s)000&user=boby&event=impression"
$ curl -vXPOST "http://localhost:3000/analytics?timestamp=$(date +%s)000&user=boby&event=click"
$ curl -vXPOST "http://localhost:3000/analytics?timestamp=$(date +%s)000&user=boby&event=impression"
$ curl -vXPOST "http://localhost:3000/analytics?timestamp=$(date +%s)000&user=boby&event=impression"
```

Get the analytics result (make sure you are using the right port

```bash
$ curl -v "http://localhost:4000/analytics?timestamp=$(date +%s)000"
```

## Teardown

```bash
$ docker compose -f docker-compose-dev.yml down
```

---
