# Networking

The basic setup (docker-compose.yml) provided `requires` the following ports to be available on your local machine to run the project!

| Component | Description              | local port required |
| --------- | ------------------------ | ------------------- |
| NGINX     | Proxy and load balancing | 8080                |
| NATS UI   | UI to "monitor" NATS     | 8282                |

The development (docker-compose-dev.yml) setup also requires the following ports to be available on your local machine to run the project!

| Component | Description                           | local port required |
| --------- | ------------------------------------- | ------------------- |
| REDIS     | In memory/fast data structure storage | 6379                |
| NATS      | Message queue                         | 4222                |
| NATS      | Message queue monitoring              | 8222                |
| NATS UI   | UI to "monitor" NATS                  | 8282                |

The toxiproxy (docker-compose-toxiproxy.yml) setup also requires the following ports to be available on your local machine to run the project!

| Component   | Description                                      | local port required |
| ----------- | ------------------------------------------------ | ------------------- |
| NATS UI     | UI to "monitor" NATS                             | 8282                |
| toxiproxy   | Proxy that can disrupt the network communication | 8474                |
| ingestion   | The POST /analytics rest API                     | 13000               |
| publication | The GET /analytics rest API                      | 14000               |
