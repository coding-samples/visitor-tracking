# Load tests

## Gatling

### Load test with only one instance of the ingression micro-service

When putting pressure on the ingestion service, we can see that the microservice (MS) starts to lags. The single thread model of NodeJS is not helping here. At around 500/rps we see that the MS unable to sustain the load and request gets queued (1.3K) and the response time is more than 1.2 seconds.

![Global result](gatling/single-ingestion-microservice/page-1.png)
![Details](gatling/single-ingestion-microservice/page-2.png)

From the shell on a mac, you can open the interactive version like this:

```bash
open docs/gatling/single-ingestion-microservice/index.html
```

### Load test when scaling ingression micro-service

Having the ingestion in its own microservice allows us to scale _ony_ the ingestion microservice (MS) and see that we can now sustain 500/rps at around 500ms.

To scale it, simply run the following command:

```bash
$ docker compose up -d --scale ingestion=2
```

![Global result](gatling/scale-2-ingestion-microservice/page-1.png)
![Details](gatling/scale-2-ingestion-microservice/page-2.png)

From the shell on a mac, you can open the interactive version like this:

```bash
open docs/gatling/scale-2-ingestion-microservice/index.html
```

Quick note to use Gatling recorder, simply prefix your favorite HTTP tool with `http_proxy=http://localhost:8000/` like this:

```bash
# go to your Gatling installation
$ cd ~/Downloads/gatling-charts-highcharts-bundle-3.6.0

# Start the recorder to record the requests made to the REST API
$ bin/recorder.sh &

# Make a couple of request like this:
$ http_proxy=http://localhost:8000/ curl -vXPOST "http://localhost:8080/analytics?timestamp=$(date +%s)000&user=bob&event=click"

# Now you can run your `generated` test with Gatling. Kewl!
$ bin/gatling.sh
```

### WIP (bonus) toxiproxy

I have included a docker-compose file `docker-compose-toxiproxy.yml` that runs all network traffic through the `toxiproxy`. You can run the `bin/load-test` to have a benchmark of the normal performance. Then if your run `bin/add-latency`, redis will take 2 seconds to respond to any request and NATS will be limited to 1kb/sec. You can re-run `bin/load-test` and see the difference. Then you can run `bin/remove-latency` to go back to normal.

NOTE: you have to stop everything before running the `docker-compose-toxiproxy.yml`!
