# Project directory structure

```
├── bin
│   └── Crude `load-tests` scripts and tools to simulate network issues
├── common
│   └── Shared `components`
├── docs
│   └── Documentations (Markdown)
├── estimation
│   └── Micro Service responsible for Estimating the number of unique users
├── gatling
│   └── Scala script to run Gatling load test.
├── ingestion
│   └── Micro Service responsible for the Endpoint that tracks users clicks/impressions
├── nginx
│   └── proxy/load balancer, helps improve http header security (and CORS)
└── publication
└── Micro Service that exposes the Endpoint to serve the tracking analytics
```
