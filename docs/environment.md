# Environment Variables

| Name                              | Description                            | Values                 | Default                 |
| --------------------------------- | -------------------------------------- | ---------------------- | ----------------------- |
| NODE_ENV                          | Running environment                    | test, dev, prod        | dev                     |
| APP_PORT                          | Server listening port                  | any number             | 3000                    |
| APP_BIND_ADDRESS                  | Interface to listen on                 | 127.0.0.1, 0.0.0.0     | 127.0.0.1               |
| APP_ORGRANIZATION                 | For white labelling                    | Apple, Microsoft       | acme                    |
| REDIS_HOST                        | hostname that run redis                | ip address or FQDN     | "127.0.0.1"             |
| REDIS_PORT                        | the port that redis is listening on    | any number             | "6379"                  |
| REDIS_PASSWORD                    | Did not have time to configure it      | a secret               | "password"              |
| NATS_STREAMING_URL                | The NATS URL                           | nats://ip_or_FQDN:port | "nats://127.0.0.1:4222" |
| NATS_STREAMING_CLUSTER_ID         | The id of the cluster                  | local, dev, qa, prod   | "local"                 |
| NATS_STREAMING_CLIENT_ID          | A unique name for each client          | consumer1, producer2   | "consumer"              |
| NATS_STREAMING_CONNECTION_TIMEOUT | How much time to wait for a connection | milliseconds           | "15000"                 |

The precedence of the environment variables are

1. The environment variable in the shell that starts the process
2. The values in the docker-compose.yml file
3. The default of the Dockerfile
4. The default in the code
