# Visitors analytics

## Problem statement

As a part of integrating with our partners, we support collecting data on website visitors and returning some basic analytics on those visitors. The goal of this task is to implement a basic endpoint for this use case. It should accept the following over HTTP.

- POST /analytics?timestamp={millis_since_epoch}&user={user_id}&event={click|impression}

  > Validations:
  >
  > - timestamp: Accept valid Instant between Instant.MIN to Instant.MAX
  > - event: "click" and "impression" only
  > - user_id: a string of 256 chars or less containing something like a hash or sha256 of something like an email.

  > HTTP/CODE returned:
  >
  > - Returns 204 with empty response when successful
  > - Returns 400 when a parameter is not valid (e.g. event is not click nor impression)
  > - Returns 404 if using a verb other than POST

- GET /analytics?timestamp={millis_since_epoch}

  > Specs:
  >
  > - The timestamp will be rounded to the start of the hour (e.g. if the timestamp given is 10h35 we will return the statistics for 10h to 11h).
  >   HTTP/CODE returned:
  >
  > HTTP/CODE returned:
  >
  > - Returns 400 when a parameter is not valid (e.g. event is not click nor impression)
  > - Returns 404 if using a verb other than POST
  > - Returns 200 with the following payload

  ```json
  {
    "unique_users": number, // number_of_unique_usernames
    "clicks": number,       // number_of_clicks
    "impressions": number   // number_of_impressions
  }
  ```

NOTE:

- It is worth noting that the traffic pattern is typical of time series data. The service will receive many more GET requests (~95%) for the current hour than for past hours (~5%). The same applies for POST requests.
  > Caching of the current hour would be a low-hanging fruit
- The **biggest challenge** that comes to mind is the sheer number of distinct user_id that we may have to handle, especially (but not only) if we have to be able to update past/current/future values of unique users.

## Hypothesis

It is **acceptable to return an estimation** of the number of unique users in real time as long as the estimation error is low ( < 1%) and that we **eventually (daily or hourly batch) correct** the estimation error.

The solution needs to be **horizontally scalable**.

## Design choices

For simplicity, I will use REDIS hyperloglog implementation (PFADD, PFCOUNT, PFMERGE) to estimate with less than 1% of error the number of unique user visits per hour and use INC to accumulate the impressions and clicks. Each one (1) hour bucket will use about 12K of memory. For one (1) day, we will need 288K. The estimation for one (1) year of data would be just over 105 MB of memory. Also, one server should support over 500 requests per second, which is a good starting point.
For load balancing in our development environment, I will use the open source version of NGINX, because of its scalability and support as ingress controller to Kubernetes.
NATS-Streaming for message bus, just because it is lighter in resources and easier to install than Kafka or Pulsar.
In development for our MVS 2, we can use Minio which is compatible with AWS S3 API, it is a drop in replacement in our local development environment.

![Proposed architecture](https://lucid.app/publicSegments/view/f256ce17-8f3f-4e4a-bb11-e6688901bb10/image.png)
