# Installation

## IMPORTANT

Make sure you can meet the following [Networking](docs/networking.md) requirements on your local machine.

## Clone the project

I assume that you have `git` already installed ;-). From a `Terminal` run the following commands.

```bash
$ git clone https://gitlab.com/coding-samples/visitor-tracking.git
$ cd visitor-tracking
```

## Docker for desktop

Docker is required to install and run the dependencies for this project, also all the configuration and networking provided in the project assume that you are running at least the infrastructure (NATS, REDIS and NGINX) in Docker. The project assume that you have the `port 8080 available` on the machine running the docker setup. If 8080 is not available, you will have to change it in the `docker-compose.yml` file and in the HTTP request you make to the REST API.

1. Follow the [instructions to download Docker for Desktop](https://www.docker.com/products/docker-desktop) on the docker website.

2. The [instructions on how to use Docker on a Mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac) can be found here.

3. On my local computer I have configured docker with 4/CPUs and 16GB/RAM. However, I think that 2/CPU and 4GB/RAM **should** be enough.

> At this point you should be able to run `docker` and either `docker-compose` or `docker compose` from a _Terminal_.

```bash
$ docker --version
Docker version 20.10.5, build 55c4c88

# Here if you install a recent version of docker you should use
# `docker compose`, but if you have an older version of docker
# already install you can dry the `docker-compose` and see if
# it's intalled
$ docker-compose --version
docker-compose version 1.29.0, build 07737305
```

---

## NodeJS & NPM

**Optionally** to `develop` locally. Not required to run the project, nor to build it from the sources.

1. [Download and install NodeJS](https://nodejs.org/en/download/) from their website.

You should be able to run `node` and `npm` from the `Terminal` at this point, if you decided that you wanted to develop locally.

```bash
$ node --version
v12.16.1

$ npm --version
7.5.6
```

---

## Other tools that can make your life easier to play with it would be

### To play with the REST API, you should have at least one of the following HTTP utilities:

> `curl` is probably already installed.

```bash
$ curl --version

# This will not work until you have deployed everything. It is just an example.
# To make an `impression` (pun intended).
$ curl -vXPOST "http://localhost:8080/analytics?timestamp=$(date +%s)000&user=bob&event=impression"

# This will not work until you have deployed everything. It is just an example.
# To get the analytics
$ curl -v "http://localhost:8080/analytics?timestamp=$(date +%s)000"
```

> `httpie` to make http requests to the REST API

```bash
# on a Mac you can install it with
$ brew install httpie

# This should give you the `usage` of httpie
$ http -v

# # This will not work until you have deployed everything. It is just an example.
$ http GET "http://localhost:4000/analytics?timestamp=$(date +%s)000"
```

> You can use [Postman](https://www.postman.com/) instead of curl or http to test the REST API.

> `jq` can also be a nice addition to pretty print the json result from the analytics GET endpoint.

```bash
# To install
$ brew install jq

# This will not work until you have deployed everything. It is just an example.
# to use it, `pipe` the result from your request to `jq` like this:
$ http GET "http://localhost:4000/analytics?timestamp=$(date +%s)000" | jq
```

---

## To test the API under load

This is not required.

## [Gatling](https://gatling.io/)

> Is my favourite tool to run test manually or via CI/CD to test my code under load and even run regression tests.
> I have included a `Gatling` scala script to beat up the REST API, it can be found under `gatling/simulations/VisitLoadTest.scala`. It was just to see how it would handle the load and if I could `scale` the ingestion endpoint.

## `ab`

> (Apache HTTP server benchmarking tool) is often already available and can be used to quickly run some load and break things!
> I have also some crude `ab` commands in `bin/load-test`, if you don't want to setup `Gatling`.

```bash
# This will not work until you have deployed everything. It is just an example.
$ ab -c 8 -n 10 -m GET http://localhost:8080/analytics\?timestamp\=0
$ ab -c 8 -n 100 -m POST http://localhost:8080/analytics\?timestamp\=0\&user\=bob\&event\=click
```

---

## Vulnerability checks

In the project I use [snyk](https://snyk.io/) while building the project with `NPN` (e.g. `npm run test-vulnerability`) and when building the docker images (e.g. `docker scan {image name}`). However, to simplify the instruction/installation you will have to register, authenticate and run this manually, sorry.

Once you have created an account with snyk, you can authenticate yourself and then the `npm run test-vulnerability` and the `docker scan {image name}` will work for you too.

```bash
$ snyk auth
```
