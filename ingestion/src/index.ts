import { app } from "./app";
import { AddressInfo } from "net";
import { natsConnect } from "./infra/nats-wrapper";
import dotenv from "dotenv";

// load the environment variables from the .env file
dotenv.config({
  path: process.env.NODE_ENV ? `.env.${process.env.NODE_ENV}` : ".env",
});

// Make sure we have a sane configuration
const config = {
  app: {
    env: process.env.NODE_ENV || "dev",
    org: process.env.APP_ORGRANIZATION || "acme",
    port: +(process.env.APP_PORT || 5000),
    bindAddress: process.env.APP_BIND_ADDRESS || "127.0.0.1",
  },
  nats: {
    url: process.env.NATS_STREAMING_URL || "nats://127.0.0.1:4222",
    clusterId: process.env.NATS_STREAMING_CLUSTER_ID || "local",
    clientId: process.env.NATS_STREAMING_CLIENT_ID || "publisher",
    timeoutInMs: +(process.env.NATS_STREAMING_CONNECTION_TIMEOUT || "15000"),
  },
};

// Connect to the NATS server
const natsClientPromise = natsConnect(config);

// Wait to be connected to the NATS server before
// anouncing that we are ready (listening).
natsClientPromise
  .then((client) => {
    app.set("nats", client);
    const server = app.listen(config.app.port, config.app.bindAddress, () => {
      const { port, address } = server.address() as AddressInfo;
      console.log(
        new Date().toISOString(),
        `[INFO ] ingestion|listening|http://${address}:${port}`
      );
    });
  })
  .catch((e) => {
    // NATS server not available. Fail fast (and furiously)
    console.log(
      new Date().toISOString(),
      `[FATAL] ingestion|connection|err=${JSON.stringify(e)}`
    );
    process.exit(-1);
  });
