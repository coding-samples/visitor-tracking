import express from "express";
import helmet from "helmet";
import addHealthRoute from "./routes/health";
import addAnalyticsRoute from "./routes/analytics";
import morgan from "morgan";

// Create the Express application
const app = express();

const testing = process.env.NODE_ENV === "test";
app.use(morgan("combined", { skip: () => testing }));

// Secure API by setting various HTTP headers.
app.use(helmet());

// Add our routes
addHealthRoute(app);
addAnalyticsRoute(app);

// Make the app available to the server and unit tests.
export { app };
