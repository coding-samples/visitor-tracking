import request from "supertest";
import { app } from "../../app";
import { minTimestampInMs, maxTimestampInMs } from "../../utils/date";

describe("GET health endpoint", () => {
  it("should return OK", async () => {
    const res = await request(app)
      .get("/health")
      .set("Accept", "*/*")
      .expect("Content-Type", /json/);
    expect(res.statusCode).toEqual(200);
    expect(res.body.ok).toEqual(true);
  });
});

describe("POST analytics endpoint", () => {
  beforeAll(() => {
    app.set("nats", { publish: () => Promise.resolve("0") });
  });
  it("should return 204 on a valid impression request", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: Date.now(),
      user: "john.doe@acme.com",
      event: "impression",
    });
    expect(res.statusCode).toEqual(204);
    expect(res.body).toEqual({});
  });

  it("should return 204 on a valid click request", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: Date.now(),
      user: "john.doe@acme.com",
      event: "click",
    });
    expect(res.statusCode).toEqual(204);
    expect(res.body).toEqual({});
  });
  it("should return 204 on with a timestamp of zero(0)", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: 0,
      user: "john.doe@acme.com",
      event: "impression",
    });
    expect(res.statusCode).toEqual(204);
    expect(res.body).toEqual({});
  });
  it("should return 204 on with a timestamp of maxTimestampInMs", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: maxTimestampInMs,
      user: "john.doe@acme.com",
      event: "impression",
    });
    expect(res.statusCode).toEqual(204);
    expect(res.body).toEqual({});
  });
  it("should return 204 on with a timestamp of minTimestampInMs", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: minTimestampInMs,
      user: "john.doe@acme.com",
      event: "impression",
    });
    expect(res.statusCode).toEqual(204);
    expect(res.body).toEqual({});
  });

  // Validate the `timestamp` parameter
  it("should return 400 when timestamp is too small", async () => {
    const res = await request(app)
      .post("/analytics")
      .query({
        timestamp: minTimestampInMs - 1,
        user: "john.doe@acme.com",
        event: "impression",
      });
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "timestamp",
        }),
      ])
    );
  });
  it("should return 400 when timestamp is too big", async () => {
    const res = await request(app)
      .post("/analytics")
      .query({
        timestamp: maxTimestampInMs + 1,
        user: "john.doe@acme.com",
        event: "impression",
      });
    expect(res.statusCode).toEqual(400);
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "timestamp",
        }),
      ])
    );
  });
  it("should return 400 when timestamp is not a numeric", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: '"1622041005830"',
      user: "john.doe@acme.com",
      event: "impression",
    });
    expect(res.statusCode).toEqual(400);
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "timestamp",
        }),
      ])
    );
  });
  it("should return 400 when timestamp is undefined", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: undefined,
      user: "john.doe@acme.com",
      event: "impression",
    });
    expect(res.statusCode).toEqual(400);
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "timestamp",
        }),
      ])
    );
  });
  it("should return 400 when timestamp is null", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: null,
      user: "john.doe@acme.com",
      event: "impression",
    });
    expect(res.statusCode).toEqual(400);
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "timestamp",
        }),
      ])
    );
  });
  it("should return 400 when timestamp is missing", async () => {
    const res = await request(app).post("/analytics").query({
      user: "john.doe@acme.com",
      event: "impression",
    });
    expect(res.statusCode).toEqual(400);
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "timestamp",
        }),
      ])
    );
  });

  // Validate the `user` parameter
  it("should return 400 when user is empty", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: Date.now(),
      user: "",
      event: "impression",
    });
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "user",
        }),
      ])
    );
  });
  it("should return 400 when user is missing", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: Date.now(),
      event: "impression",
    });
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "user",
        }),
      ])
    );
  });
  it("should return 400 when user is undefined", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: Date.now(),
      user: undefined,
      event: "impression",
    });
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "user",
        }),
      ])
    );
  });
  it("should return 400 when user is null", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: Date.now(),
      user: null,
      event: "impression",
    });
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "user",
        }),
      ])
    );
  });

  // Validate the `event` parameter
  it("should return 400 when event is missing", async () => {
    const res = await request(app).post("/analytics").query({
      user: "john.doe@acme.com",
      timestamp: Date.now(),
    });
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "event",
        }),
      ])
    );
  });
  it("should return 400 when event is undefined", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: Date.now(),
      user: "john.doe@acme.com",
      event: undefined,
    });
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "event",
        }),
      ])
    );
  });
  it("should return 400 when event is null", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: Date.now(),
      user: "john.doe@acme.com",
      event: null,
    });
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "event",
        }),
      ])
    );
  });
  it("should return 400 when event is numeric", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: Date.now(),
      user: "john.doe@acme.com",
      event: 42,
    });
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "event",
        }),
      ])
    );
  });
  it("should return 400 when event is not impression nor click", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: Date.now(),
      user: "john.doe@acme.com",
      event: "bob was here",
    });
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "event",
        }),
      ])
    );
  });
  it("should return 400 when trying simple JSON injection", async () => {
    const res = await request(app).post("/analytics").query({
      timestamp: Date.now(),
      user: "john.doe@acme.com",
      event: 'click","msg":"you are vulnerable"',
    });
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "event",
        }),
      ])
    );
  });
  it("should return 400 when no parameters are passed", async () => {
    const res = await request(app).post("/analytics");
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(3);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "timestamp",
        }),
        expect.objectContaining({
          field: "user",
        }),
        expect.objectContaining({
          field: "event",
        }),
      ])
    );
  });

  // afterAll(async () => {
  //   try {
  //     await shutdown();
  //   } catch (e) {
  //     console.log(e);
  //   }
  //   return;
  // });
});
