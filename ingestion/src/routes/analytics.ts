import { Express } from "express";
import { Visit, Event } from "../models/Visit";
import QueryParam from "../models/QueryParam";
import { isEmpty, isString, isTimestampValid } from "../utils/validators";

const TOPIC_VISIT = "analytics.visit";

// an Event must be either `click` or `impression`
const isEventValid = (event: QueryParam) =>
  event === "click" || event === "impression";

// a user must be of defined, not-null (isString) and non empty
const isUserPresent = (user: QueryParam) => isString(user) && !isEmpty(user);

export default (app: Express): Express => {
  app.post("/analytics", (req, res) => {
    // Construct a list of validation failures, just in case (not optimal)
    const validationErrors: {
      ok: boolean;
      errors: { field: string; msg: string; value: string }[];
    } = {
      ok: false,
      errors: [],
    };

    if (!isTimestampValid(req.query.timestamp)) {
      validationErrors.errors.push({
        field: "timestamp",
        msg: "timestamp is required and must convert to a valid date object",
        value: `${req.query.timestamp}`,
      });
    }

    if (!isUserPresent(req.query.user)) {
      validationErrors.errors.push({
        field: "user",
        msg: "user is required and must be a non empty string",
        value: `${req.query.user}`,
      });
    }

    if (!isEventValid(req.query.event)) {
      validationErrors.errors.push({
        field: "event",
        msg: "event is required and must be either `click` or `impression`",
        value: `${req.query.event}`,
      });
    }

    // If we did not pass the validation, return all the failures
    if (validationErrors.errors.length > 0) {
      return res.status(400).json(validationErrors);
    }

    // Publish the visit to the topic before
    // returning success, we are returnin a 204 not a 202!
    const visit: Visit = {
      timestamp: +(req.query.timestamp as string),
      user: req.query.user as string,
      event: req.query.event as Event,
    };

    const nats = req.app.get("nats");
    return nats
      .publish(TOPIC_VISIT, visit)
      .then(() => res.status(204).send()) // all good, it is owned by NATS now
      .catch(() => {
        res.status(500).json({ ok: false, msg: "Unable to process visit" });

        // WARNING:  Fail Fast here
        // -------
        // There are not any elegant way of hangling
        // NATS failures (connection lost mostly).
        // Therefore, exiting at letting the orchestrator
        // restart the microservice is probably OK. Furthermore,
        // while the service is not responding, the loadbalancer
        // will send the request to a working node.
        // Counter point to exiting: if NATS server restart, every
        // Microservice will die and restart at the same time...
        // It could be ugly!
        process.exit(-1);
      });
  });
  return app;
};
