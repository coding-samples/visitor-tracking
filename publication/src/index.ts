import { app } from "./app";
import { AddressInfo } from "net";
import redisConnect from "./infra/redis-wrapper";
import dotenv from "dotenv";
import { stats } from "./visit/stats";

// load the environment variables from the .env file
dotenv.config({
  path: process.env.NODE_ENV ? `.env.${process.env.NODE_ENV}` : ".env",
});

// Make sure we have a sane configuration
const config = {
  app: {
    env: process.env.NODE_ENV || "dev",
    org: process.env.APP_ORGRANIZATION || "acme",
    port: +(process.env.APP_PORT || 4000),
    bindAddress: process.env.APP_BIND_ADDRESS || "127.0.0.1",
  },
  redis: {
    host: process.env.REDIS_HOST || "127.0.0.1",
    port: +(process.env.REDIS_PORT || "6379"),
    password: process.env.REDIS_PASSWORD || "password",
  },
};

const redisClientPromise = redisConnect(config.redis.host, config.redis.port);
// Wait for REDIS to be ready before
// anouncing that we are available to serve the analytics endpoint (listening).
redisClientPromise
  .then((redisClient) => {
    app.set("stats", stats(config.app, redisClient));
    const server = app.listen(config.app.port, config.app.bindAddress, () => {
      const { port, address } = server.address() as AddressInfo;
      console.log(
        new Date().toISOString(),
        `[INFO ] publication|listening|http://${address}:${port}`
      );
    });
  })
  .catch((e) => {
    // REDIS server not available. Fail fast (and furiously)
    console.log(
      new Date().toISOString(),
      `[FATAL] publication|connection|err=${JSON.stringify(e)}`
    );
    process.exit(-1);
  });
