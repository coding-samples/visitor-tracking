import redis from "redis";
import VisitStats from "../models/VisitStats";
import Config from "../models/Config";
import { timestamp2bucket } from "../utils/date";

const stats =
  (appConfig: Config, client: redis.RedisClient) =>
  (timestamp: number): Promise<VisitStats> =>
    new Promise<VisitStats>((resolve, reject) => {
      // The visit `timestamp` is safe, because it has been converted to a number.
      // The `event` has also been validated to be only 'click' or 'impression'.
      // The `user` on the other hand has *not* been sanitized, but since we are
      // using it only as a parameter to a hash function we can keep it as-is.
      const bucket = timestamp2bucket(timestamp);
      const userBucketKey = `${appConfig.org}:${appConfig.env}:visit:user:${bucket}`;
      const clickBucketKey = `${appConfig.org}:${appConfig.env}:visit:click:${bucket}`;
      const impressionBucketKey = `${appConfig.org}:${appConfig.env}:visit:impression:${bucket}`;

      // Run pfadd and incr in a transaction to make sure that when
      // the consumer API present the information is in a consistent
      // state.
      client
        .multi()
        .pfcount(userBucketKey)
        .get(clickBucketKey)
        .get(impressionBucketKey)
        .exec((err, replies) => {
          if (err) {
            return reject(err);
          }
          // Our transcation contains two (3) commands to execute,
          // we expect to receive two (3) results from REDIS.
          // In science we trust, all others have to pay cash!
          if (replies.length !== 3) {
            console.log(
              new Date().toISOString(),
              `[WARN ] publication|transactions|expected=3|got=${replies.length}`
            );
          }
          const result: VisitStats = {
            unique_users: +replies[0], // Convert string to number
            clicks: +replies[1], // Can be either a string or null when no value
            impressions: +replies[2], // Can be either a string or null when no value
          };
          console.log(
            new Date().toISOString(),
            `[INFO ] publication|estimate|timestamp=${timestamp}|bucket=${bucket}|stats=${JSON.stringify(
              result
            )}`
          );
          resolve(result);
        });
    });

export { stats, timestamp2bucket };
