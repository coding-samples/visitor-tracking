import { Express } from "express";
import { isTimestampValid } from "../utils/validators";
import VisitStats from "../models/VisitStats";

export default (app: Express): Express => {
  app.get("/analytics", (req, res) => {
    const validationErrors: {
      ok: boolean;
      errors: { field: string; msg: string; value: string }[];
    } = {
      ok: false,
      errors: [],
    };
    if (!isTimestampValid(req.query.timestamp)) {
      validationErrors.errors.push({
        field: "timestamp",
        msg: "timestamp is required and must convert to a valid date object",
        value: `${req.query.timestamp}`,
      });
    }
    if (validationErrors.errors.length > 0) {
      return res.status(400).json(validationErrors);
    }

    const stats = req.app.get("stats");
    return stats(+(req.query.timestamp as string))
      .then((result: VisitStats) => res.status(200).json(result))
      .catch(() => {
        res.status(500).json({ ok: false, msg: "Unable to get the stats" });
        // WARNING:  Fail Fast here
        // -------
        // There are not any elegant way of handling
        // REDIS failures (connection lost mostly).
        // Therefore, exiting at letting the orchestrator
        // restart the microservice is probably OK. Furthermore,
        // while the service is not responding, the loadbalancer
        // will send the request to a working node.
        // Counter point to exiting: if REDIS server restart, every
        // Microservice will die and restart at the same time...
        // It could be ugly!
        process.exit(-1);
      });
  });
  return app;
};
