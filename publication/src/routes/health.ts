import { Express } from "express";

export default (app: Express): Express => {
  app.get("/health", (req, res) => res.json({ ok: true }));
  return app;
};
