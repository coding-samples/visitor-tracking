import request from "supertest";
import { app } from "../../app";
import { minTimestampInMs, maxTimestampInMs } from "../../utils/date";

describe("GET health endpoint", () => {
  it("should return OK", async () => {
    const res = await request(app)
      .get("/health")
      .set("Accept", "*/*")
      .expect("Content-Type", /json/);
    expect(res.statusCode).toEqual(200);
    expect(res.body.ok).toEqual(true);
  });
});

describe("GET analytics endpoint", () => {
  beforeAll(() => {
    app.set("stats", () =>
      Promise.resolve({
        unique_users: 1,
        clicks: 2,
        impressions: 3,
      })
    );
  });

  it("should return 200 on with a timestamp of zero(0)", async () => {
    const res = await request(app).get("/analytics").query({ timestamp: 0 });
    expect(res.statusCode).toBe(200);
    expect(res.body.unique_users).toEqual(1);
    expect(res.body.clicks).toEqual(2);
    expect(res.body.impressions).toEqual(3);
  });

  it("should return 200 on with a timestamp of maxTimestampInMs", async () => {
    const res = await request(app)
      .get("/analytics")
      .query({ timestamp: maxTimestampInMs });
    expect(res.statusCode).toEqual(200);
    expect(res.body.unique_users).toEqual(1);
    expect(res.body.clicks).toEqual(2);
    expect(res.body.impressions).toEqual(3);
  });
  it("should return 200 on with a timestamp of minTimestampInMs", async () => {
    const res = await request(app)
      .get("/analytics")
      .query({ timestamp: minTimestampInMs });
    expect(res.statusCode).toEqual(200);
    expect(res.body.unique_users).toEqual(1);
    expect(res.body.clicks).toEqual(2);
    expect(res.body.impressions).toEqual(3);
  });

  // Validate the `timestamp` parameter
  it("should return 400 when timestamp is too small", async () => {
    const res = await request(app)
      .get("/analytics")
      .query({ timestamp: minTimestampInMs - 1 });
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "timestamp",
        }),
      ])
    );
  });

  it("should return 400 when timestamp is too big", async () => {
    const res = await request(app)
      .get("/analytics")
      .query({ timestamp: maxTimestampInMs + 1 });
    expect(res.statusCode).toEqual(400);
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "timestamp",
        }),
      ])
    );
  });
  it("should return 400 when timestamp is not a numeric", async () => {
    const res = await request(app)
      .get("/analytics")
      .query({ timestamp: '"1622041005830"' });
    expect(res.statusCode).toEqual(400);
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "timestamp",
        }),
      ])
    );
  });
  it("should return 400 when timestamp is undefined", async () => {
    const res = await request(app)
      .get("/analytics")
      .query({ timestamp: undefined });
    expect(res.statusCode).toEqual(400);
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "timestamp",
        }),
      ])
    );
  });
  it("should return 400 when timestamp is null", async () => {
    const res = await request(app).get("/analytics").query({ timestamp: null });
    expect(res.statusCode).toEqual(400);
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "timestamp",
        }),
      ])
    );
  });
  it("should return 400 when timestamp is missing", async () => {
    const res = await request(app).get("/analytics").query({});
    expect(res.statusCode).toEqual(400);
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "timestamp",
        }),
      ])
    );
  });
  it("should return 400 when no parameters are passed", async () => {
    const res = await request(app).get("/analytics");
    expect(res.statusCode).toEqual(400);
    expect(res.body.ok).toEqual(false);
    expect(res.body.errors.length).toEqual(1);
    expect(res.body.errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          field: "timestamp",
        }),
      ])
    );
  });
});
