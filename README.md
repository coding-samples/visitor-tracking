# Visitor Tracking

## Table of content

- [Problem statement](docs/problem-statement.md)
- [Installation](docs/installation.md)
- [Directory structure of the project](docs/directory-structure.md)
- [Environment variables](docs/environment.md)
- [Networking](docs/networking.md)
- [Non exaustive list of things to be done](TODO.md)
- [Running the proof of concept](docs/instructions.md)
- [Gatling load test results](docs/loadtest.md)
- [References](docs/references.md)
