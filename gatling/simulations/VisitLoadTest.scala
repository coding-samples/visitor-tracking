
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._


class VisitLoadTest extends Simulation {
	val random = new util.Random
	def randomEvent = if (random.nextBoolean()) "click" else "impression"
	val feeder = Iterator.continually(
    Map(
      "user" -> java.util.UUID.randomUUID.toString,
			"timestamp" -> System.currentTimeMillis(),
			"event" -> randomEvent
    )
  )

	val httpProtocol = http
		.baseUrl("http://localhost:8080")
		.proxy(Proxy("localhost", 8080))
		.acceptHeader("*/*")
		.userAgentHeader("curl/7.54.0")

	val headers = Map("Proxy-Connection" -> "Keep-Alive")

	val scn = scenario("VisitLoadTest")
		.exec(
			exec(feed(feeder)),
			exec(http("random-event")
				.post("/analytics?timestamp=${timestamp}&user=${user}&event=${event}")
				.headers(headers))
		)
		
	// setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)
	setUp(
		scn.inject(
			nothingFor(1.seconds),
			atOnceUsers(100),
			rampUsers(100).during(5.seconds), 
			constantUsersPerSec(100).during(15.seconds),
			constantUsersPerSec(100).during(15.seconds).randomized,
			// rampUsersPerSec(10).to(20).during(10.minutes),
			// rampUsersPerSec(10).to(20).during(10.minutes).randomized,
			heavisideUsers(4000).during(20.seconds)
		)
	).protocols(httpProtocol)
	
}