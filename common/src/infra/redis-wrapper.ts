import redis from "redis";

const redisConnect = (
  host: string,
  port: number,
  password?: string,
  timeoutInMs = 1500
): Promise<redis.RedisClient> =>
  new Promise((resolve, reject) => {
    const client = redis.createClient({
      host,
      port,
      password,
    });

    const timeoutHandle = setTimeout(() => {
      reject(`REDIS connection timeout after ${timeoutInMs / 1000} secdonds`);
      process.exit(-1);
    }, timeoutInMs);

    // TODO: promise can already be resolved/rejected!
    //       for now, crashing is "acceptable" because we
    //       will be restarted by docker
    client.on("error", (error) => {
      clearTimeout(timeoutHandle);
      return reject(`REDIS on error: ${JSON.stringify(error)}`);
    });

    client.on("ready", () => {
      clearTimeout(timeoutHandle);
      return resolve(client);
    });
  });

export default redisConnect;
