import { hostname } from "os";
import nats from "node-nats-streaming";
import Config from "../models/Config";

// This is an opinionated topic nomenclature to support
// - multi-tenant (org name for white labeling)
// - multi-environement (environement)
const topicBuilder = (topic: string, appConfig: Config): string =>
  `${appConfig.org}.${appConfig.env}.${topic}`;

// Wrapper on the nats client to encapsulate the client with promises
// and enforce the nomenclature of the topics
class NatsClient {
  config: Config;
  client: nats.Stan;

  constructor(config: Config, client: nats.Stan) {
    this.config = config;
    this.client = client;
  }

  publish(topic: string, message: Record<string, unknown>): Promise<string> {
    return new Promise((resolve, reject) =>
      this.client.publish(
        topicBuilder(topic, this.config.app),
        JSON.stringify(message),
        (error, guid) => {
          if (error) {
            return reject(error);
          }
          return resolve(guid);
        }
      )
    );
  }

  subscribe(
    topic: string,
    durableName: string,
    callback: (data: string, seq: number) => Promise<void>
  ): void {
    const opts = this.client
      .subscriptionOptions()
      .setDurableName(durableName)
      .setManualAckMode(true);
    const subscription = this.client.subscribe(
      topicBuilder(topic, this.config.app),
      opts
    );
    subscription.on("message", (msg) => {
      callback(msg.getData(), msg.getSequence())
        .then(() => msg.ack())
        .catch((e) =>
          console.log(
            new Date().toISOString(),
            `[FATAL] NATS|consumer-exception|err=${JSON.stringify(e)}`
          )
        );
    });
  }
}

const natsConnect = (config: Config): Promise<NatsClient> =>
  new Promise((resolve, reject) => {
    // Build a multi-tenant, multi-env, scalable and valid clientID
    const clientId = `${config.app.org}-${config.app.env}-${
      config.nats.clientId
    }-${hostname().replace(/[^a-zA-Z0-9_-]+/g, "_")}`;
    const natsclient = nats.connect(config.nats.clusterId, clientId, {
      url: config.nats.url,
    });

    // Fail fast in case we are unable to connect
    const timeoutHandle = setTimeout(() => {
      reject(
        `NATS connection timeout after ${
          config.nats.timeoutInMs / 1000
        } secdonds`
      );
      process.exit(-1);
    }, config.nats.timeoutInMs);

    // TODO: promise can already be resolved/rejected!
    //       for now, crashing is "acceptable" because we
    //       will be restarted by docker
    natsclient.on("error", (error) => {
      clearTimeout(timeoutHandle);
      return reject(`NATS on error: ${JSON.stringify(error)}`);
    });

    natsclient.on("connect", () => {
      clearTimeout(timeoutHandle);
      return resolve(new NatsClient(config, natsclient));
    });
  });

export { NatsClient, natsConnect, topicBuilder };
