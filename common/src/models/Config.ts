// The configuration read from the environment
// variables.

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Config = { [key: string]: any };

export default Config;
