// The type for HTTP Query parameters

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type QueryParam = any;

export default QueryParam;
