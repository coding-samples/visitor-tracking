// The message that is produce by the REST API and
// published to the message bus.

type Event = "click" | "impression";
type Visit = {
  timestamp: number;
  user: string;
  event: Event;
};

export { Event, Visit };
