// The contract with the consumers of our REST API
//
type VisitStats = {
  unique_users: number; // number_of_unique_usernames
  clicks: number; // number_of_clicks
  impressions: number; // number_of_impressions
};

export default VisitStats;
