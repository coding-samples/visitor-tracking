import { minTimestampInMs, maxTimestampInMs, timestamp2bucket } from "../date";

describe("Javascript Date Object maximum and minimum values supported", () => {
  it("maxTimestampInMs should be greater than now", async () => {
    expect(maxTimestampInMs).toBeGreaterThan(Date.now());
  });
  it("minTimestampInMs should be less than now", async () => {
    expect(minTimestampInMs).toBeLessThan(Date.now());
  });
});

describe("Algorithm to generate a Bucket key from a timestamp", () => {
  it("should always be 11 chars in length", async () => {
    const bucket = timestamp2bucket(Date.now());
    expect(bucket.length).toEqual(11);
  });
  it("should be 8 digits followed by `:` and 2 digits", async () => {
    const bucket = timestamp2bucket(Date.now());
    expect(bucket).toMatch(/^\d{8}:\d{2}$/);
  });
});
