import { minTimestampInMs, maxTimestampInMs } from "../date";
import { isEmpty, isString, isDate, isTimestampValid } from "../validators";

describe("HTTP Query Parameters validators", () => {
  it("isEmpty should return true on an empty string", async () => {
    expect(isEmpty("")).toBe(true);
  });
  it("isEmpty should return false on a non empty string", async () => {
    expect(isEmpty("Hello")).toBe(false);
  });
  it("isString should return false on null", async () => {
    expect(isString(null)).toBe(false);
  });
  it("isString should return false on undefined", async () => {
    expect(isString(undefined)).toBe(false);
  });
  it("isString should return false on a number", async () => {
    expect(isString(42)).toBe(false);
  });
  it("isString should return false on an object", async () => {
    expect(isString({})).toBe(false);
  });
  it("isString should return false on an array", async () => {
    expect(isString([])).toBe(false);
  });

  it("isDate should return true on a value of minTimestamp", async () => {
    expect(isDate(minTimestampInMs)).toBe(true);
  });
  it("isDate should return true on a value of maxTimestamp", async () => {
    expect(isDate(maxTimestampInMs)).toBe(true);
  });
  it("isDate should return true on a value of 0", async () => {
    expect(isDate(0)).toBe(true);
  });
  it("isDate should return true on a value of now", async () => {
    expect(isDate(Date.now())).toBe(true);
  });
  it("isDate should return false on a value of minTimestamp -1 ", async () => {
    expect(isDate(minTimestampInMs - 1)).toBe(false);
  });
  it("isDate should return false on a value of maxTimestamp +1 ", async () => {
    expect(isDate(maxTimestampInMs + 1)).toBe(false);
  });

  it("isTimestampValid should return true on a value of minTimestamp", async () => {
    expect(isTimestampValid("" + minTimestampInMs)).toBe(true);
  });
  it("isTimestampValid should return true on a value of maxTimestamp", async () => {
    expect(isTimestampValid("" + maxTimestampInMs)).toBe(true);
  });
  it("isTimestampValid should return true on a value of 0", async () => {
    expect(isTimestampValid("0")).toBe(true);
  });
  it("isTimestampValid should return true on a value of now", async () => {
    expect(isTimestampValid("" + Date.now())).toBe(true);
  });
  it("isTimestampValid should return false on a value of minTimestamp -1 ", async () => {
    expect(isTimestampValid("" + (minTimestampInMs - 1))).toBe(false);
  });
  it("isTimestampValid should return false on a value of maxTimestamp +1 ", async () => {
    expect(isTimestampValid("" + (maxTimestampInMs + 1))).toBe(false);
  });

  it("isTimestampValid should return false on null", async () => {
    expect(isTimestampValid(null)).toBe(false);
  });
  it("isTimestampValid should return false on undefined", async () => {
    expect(isTimestampValid(undefined)).toBe(false);
  });
  it("isTimestampValid should return false on an empty string", async () => {
    expect(isTimestampValid("")).toBe(false);
  });
  it("isTimestampValid should return false on an object", async () => {
    expect(isTimestampValid({})).toBe(false);
  });
  it("isTimestampValid should return false on an array", async () => {
    expect(isTimestampValid([])).toBe(false);
  });
});
