import QueryParam from "../models/QueryParam";

// Must not be an empty string
const isEmpty = (value: string): boolean => value === "";

// Must be of type string, of defined value and not null
const isString = (value: QueryParam): boolean =>
  Object.prototype.toString.apply(value) === "[object String]";

// Must be able to construct a valid Date object
const isDate = (timestamp: number): boolean =>
  new Date(timestamp).toString() !== "Invalid Date";

// Must be able to create a valide Date object from the timestamp
const isTimestampValid = (timestamp: QueryParam): boolean =>
  isString(timestamp) && !isEmpty(timestamp) && isDate(+timestamp);

export { isEmpty, isString, isDate, isTimestampValid };
