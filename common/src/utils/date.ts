// extreme values supported by the Date() object.
const maxTimestampInMs = 8640000000000000;
const minTimestampInMs = -8640000000000000;

const timestamp2bucket = (timestamp: number): string =>
  new Date(timestamp)
    .toISOString() // 2021-05-28T21:28:31.316Z
    .replace(/-/g, "") // 20210528T21:28:31.316Z
    .replace(/T/, ":") // 20210528:21:28:31.316Z
    .slice(0, 11); // 20210528:21

export { maxTimestampInMs, minTimestampInMs, timestamp2bucket };
