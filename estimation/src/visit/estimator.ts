import redis from "redis";
import { Visit } from "../models/Visit";
import { timestamp2bucket } from "../utils/date";

const estimate =
  (appConfig: { [k: string]: string }, client: redis.RedisClient) =>
  (visit: Visit): Promise<void> =>
    new Promise<void>((resolve, reject) => {
      // The visit `timestamp` is safe, because it has been converted to a number.
      // The `event` has also been validated to be only 'click' or 'impression'.
      // The `user` on the other hand has *not* been sanitized, but since we are
      // using it only as a parameter to a hash function we can keep it as-is.
      const bucket = timestamp2bucket(visit.timestamp);
      const userBucketKey = `${appConfig.org}:${appConfig.env}:visit:user:${bucket}`;
      const eventBucketKey = `${appConfig.org}:${appConfig.env}:visit:${visit.event}:${bucket}`;

      // Run pfadd and incr in a transaction to make sure that when
      // the consumer API present the information is in a consistent
      // state.
      client
        .multi()
        .pfadd(userBucketKey, visit.user)
        .incr(eventBucketKey)
        .exec((err, replies) => {
          if (err) {
            return reject(err);
          }
          // Our transcation contains two (2) commands to execute,
          // we expect to receive two (2) results from REDIS.
          // In science we trust, all others have to pay cash!
          if (replies.length !== 2) {
            console.log(
              new Date().toISOString(),
              `[WARN ] estimation|transactions|expected=2|got=${replies.length}`
            );
          }
          console.log(
            new Date().toISOString(),
            `[INFO ] estimation|bucket|${userBucketKey}=+${replies[0]}|${eventBucketKey}=${replies[1]}`
          );
          resolve();
        });
    });

export { estimate, timestamp2bucket };
