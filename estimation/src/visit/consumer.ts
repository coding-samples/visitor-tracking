import { RedisClient } from "redis";
import { NatsClient } from "../infra/nats-wrapper";
import { estimate } from "../visit/estimator";
import { Visit } from "../models/Visit";
import Config from "../models/Config";

const TOPIC_VISIT = "analytics.visit";

// use to keep track of the last message processed
const TOPIC_DURABLE = `${TOPIC_VISIT}-estimator`;

const consumer = (
  appConfig: Config,
  natsClient: NatsClient,
  redisClient: RedisClient
): void => {
  const visitEstimator = estimate(appConfig, redisClient);
  natsClient.subscribe(TOPIC_VISIT, TOPIC_DURABLE, (data: string) => {
    const visit: Visit = JSON.parse(data);
    return visitEstimator(visit);
  });
};

export { consumer };
