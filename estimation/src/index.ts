import dotenv from "dotenv";
import { natsConnect } from "./infra/nats-wrapper";
import redisConnect from "./infra/redis-wrapper";
import { consumer } from "./visit/consumer";

// load the environment variables from the .env file
dotenv.config({
  path: process.env.NODE_ENV ? `.env.${process.env.NODE_ENV}` : ".env",
});

// Make sure we have a sane configuration
const config = {
  app: {
    env: process.env.NODE_ENV || "dev",
    org: process.env.APP_ORGRANIZATION || "acme",
  },
  redis: {
    host: process.env.REDIS_HOST || "127.0.0.1",
    port: +(process.env.REDIS_PORT || "6379"),
    password: process.env.REDIS_PASSWORD || "password",
  },
  nats: {
    url: process.env.NATS_STREAMING_URL || "nats://127.0.0.1:4222",
    clusterId: process.env.NATS_STREAMING_CLUSTER_ID || "local",
    clientId: process.env.NATS_STREAMING_CLIENT_ID || "consumer",
    timeoutInMs: +(process.env.NATS_STREAMING_CONNECTION_TIMEOUT || "15000"),
  },
};

// Connect to the NATS server
const natsClientPromise = natsConnect(config);

// Connect to the REDIS server
const redisClientPromise = redisConnect(config.redis.host, config.redis.port);

// Wait for REDIS and NATS to be ready before we start
// processing the visits.
Promise.all([natsClientPromise, redisClientPromise])
  .then(([natsClient, redisClient]) => {
    console.log(new Date().toISOString(), "[INFO ] estimation|ready");
    consumer(config.app, natsClient, redisClient);
  })
  .catch((e) => {
    // NATS server not available. Fail fast (and furiously)
    console.log(
      new Date().toISOString(),
      `[FATAL] estimation|connection|err=${JSON.stringify(e)}`
    );
    process.exit(-1);
  });
